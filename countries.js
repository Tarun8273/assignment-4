const countriesElem = document.querySelector(".countries");
const dropDown = document.querySelector(".dropDown");
const dropElem = document.querySelector(".drop");
//for all region countries
const region = document.querySelectorAll(".region");
//only search country
const search = document.querySelector(".search");
///for country details
const detailCountry = document.querySelector(".countries");
//





async function getCountry() {
    const url = await fetch("https://restcountries.com/v2/all");
    const res = await url.json();
    console.log(res);
    res.forEach(element => {
        showCountry(element);
    });
}
getCountry()


//${data.currencies.map(elem=>elem.name)}
function showCountry(data) {
    const country = document.createElement("div");
    country.classList.add("country");

    // const countryDetailButton = document.createElement("div");
    // countryDetailButton.classList.add("cmodal");

    country.innerHTML = ` 
<div class="country-info" >
    <div class="country-img">
    <img src="${data.flag}" alt="">
    </div>
    <div class="cmodal">
    <h3 class="countryName">${data.name}</h3>
    <p class="regionName"><strong>Region:</strong>${data.region}</p>
    <p><strong>Current date and time: </strong>${data.timezones}</p>
    <a href="https://www.google.com/maps/place/${data.name}"><button type="button"class="map">Show Map</button></a>&nbsp;&nbsp;
    <a href="./detail.html?Country_ID=${data.alpha3Code}"><button type="button"class="map">View Detail</button></a>&nbsp;&nbsp;
    </div>
</div> `;
     
    
    
    //console.log(countryDetailButton)
    countriesElem.appendChild(country);
    //country.appendChild(countryDetailButton);

    // countryDetailButton.addEventListener("click",()=>{
    //     showCountryDetailByName(data);
    // })
    
}




//borders


//<a href="url">link text</a>
dropDown.addEventListener("click", () => {
    dropElem.classList.toggle("showDropDown")
    console.log("hello");
})

//for Region
const regionName = document.getElementsByClassName("regionName")
region.forEach(element => {
    element.addEventListener("click", () => {
        console.log(element);
        Array.from(regionName).forEach(elem => {
            console.log(elem.innerText);

            if (elem.innerText.includes(element.innerText) || element.innerText == "All") {
                elem.parentElement.parentElement.parentElement.style.display = "grid";
            } else {
                elem.parentElement.parentElement.parentElement.style.display = "none";
            }
        });
    })
});

//for Search any country by name
search.addEventListener('input', e => {
    const { value } = e.target;
    //console.log(value);
    const countryName = document.querySelectorAll(".countryName")

    countryName.forEach(name => {
        console.log(name.innerText);
        if (name.innerText.toLowerCase().includes(value.toLowerCase())) {
            name.parentElement.parentElement.parentElement.style.display = "grid";
        } else {
            name.parentElement.parentElement.parentElement.style.display = "none";
        }
    })
    // Array.from(countryName).forEach(elem => {
    //    // console.log(elem.innerText);
    //     if (elem.innerText.toLowerCase().includes(search.value.toLowerCase())) {
    //         elem.parentElement.parentElement.style.display = "grid";
    //     } else {
    //         elem.parentElement.parentElement.style.display = "none";
    //     }
    // });
})



//for country detail page

//for back btn
// const back = document.querySelector(".back")
// const countryModal = document.querySelector(".countryModal")
// back.addEventListener("click", ()=>{
//     countryModal.classList.toggle("show");
// })


//for countryDetailButton


//const showdetailbutton=document.getElementById("cbutton")
//console.log(showdetailbutton);
//countryModal
//const countryModal = document.querySelectorAll(".countryModal")
//console.log(countryModal)

//showdetailbutton.addEventListener("click",showCountryDetails,false);
     
// function showCountryDetails() {
//     console.log("aa")
//     const countryModal = document.querySelectorAll(".countryModal")
//    //const countryModal = document.getElementById("hero")
//     console.log(countryModal)
// }





//<p><strong>Currency: </strong>${data.currencies.map(elem => elem.name)}</p>
//https://restcountries.com/v3.1/all
//https://restcountries.com/v2/all

const countryModal=document.querySelector(".countryModal");
//console.log(countryModal);

const neghibourCountries = document.querySelector(".ncmodal");
console.log(neghibourCountries);

onload=view;
function view(Country_ID) {
   
    fetch(`https://restcountries.com/v2/all`)
      .then(res => res.json())
      .then(data => display_detail(data))
  }
  function display_detail(data) 
  {
    function getParameterByName(name, url = window.location.href) {
        name = name.replace(/[\[\]]/g, '\\$&');
        var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, ' '));
        }
        let exp=getParameterByName("Country_ID");
        data.forEach(ctry => {
            if(ctry.alpha3Code==exp)
            {
                
                countryModal.classList.toggle("show")
                countryModal.innerHTML = `
                <div class="modal">
                <h2 class="h2tag">${ctry.name}</h2>
                <div class="leftModal">
                <div class="imgflag">
                    <img src="${ctry.flag}" alt="">
                    </div>
             
                <div class="rightModal">
                            <p><strong>Native Name: </strong>${ctry.nativeName}</p>
                            <p><strong>Capital: </strong>${ctry.capital}</p>
                            <p><strong>Population: </strong>${ctry.population}</p>   
                            <p><strong>Region: </strong>${data.region}</p>
                            <p><strong>Sub-Region: </strong>${data.subregion}</p>
                            <p><strong>Area: </strong>${ctry.area}</p>
                            <p><strong>Country Code: </strong>${ctry.alpha3Code}</p>
                            <p><strong>Languages: </strong>${ctry.languages.map(elem=>elem.name)}</p>
                            <p><strong>Currencies: </strong>${ctry.currencies.map(elem=>elem.name)}</p>
                             <p><strong>Timezones: </strong>${ctry.timezones}</p> 
                             <p><strong>Current Time: </strong>${new Date().toLocaleString("en-US",{timeZone: 'Europe/London',timeStyle:'medium',hourCycle:'h12'})}</p> 
                </div>
             
                </div>
             </div>`
              
             const abc = ctry.borders;
            data.forEach(ctry =>{ 
                for(i=0;i<abc.length;i=i+1)
                {
                    if(abc[i] == ctry.alpha3Code)
                    {
                      img=document.createElement("img");
                      img.src=ctry.flag;
                      img.className="neighbour_img"
                      neghibourCountries.appendChild(img);
                    }
                }
            })      
  }
});
}
